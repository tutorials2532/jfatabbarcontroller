//
//  JFATabBarController.h
//  JFATabBarController
//
//  Created by Josh Adams on 5/18/13.
//  Copyright (c) 2013 Josh Adams. All rights reserved.
//

// Copyright belongs to original author
// http://code4app.net (en) http://code4app.com (cn)
// From the most professional code share website: Code4App.net

@interface JFATabBarController : UITabBarController
@end
