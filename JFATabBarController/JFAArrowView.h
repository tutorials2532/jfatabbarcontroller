//
//  JFAArrowView.h
//  JFATabBarController
//
//  Created by Josh Adams on 8/3/14.
//  Copyright (c) 2014 Josh Adams. All rights reserved.
//

// Copyright belongs to original author
// http://code4app.net (en) http://code4app.com (cn)
// From the most professional code share website: Code4App.net

#import <UIKit/UIKit.h>

@interface JFAArrowView : UIView
typedef enum
{
	ARROW_DIRECTION_LEFT = 0,
	ARROW_DIRECTION_RIGHT = 1,
} ArrowDirection;
- (JFAArrowView *)initWithDirection:(ArrowDirection)arrowDirection;
- (void)fadeIn;
- (void)fadeOut;
@end
