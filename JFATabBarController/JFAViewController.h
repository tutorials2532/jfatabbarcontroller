//
//  JFAViewController.h
//  JFATabBarController
//
//  Created by Joshua Adams on 8/2/14.
//  Copyright (c) 2014 Josh Adams. All rights reserved.
//

// Copyright belongs to original author
// http://code4app.net (en) http://code4app.com (cn)
// From the most professional code share website: Code4App.net

#import <UIKit/UIKit.h>

@interface JFAViewController : UIViewController

@end
